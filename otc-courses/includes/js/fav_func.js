		/** Edited by Louis F. **/
//var $ = jQuery.noConflict( true );
window.jQuery = window.$ = jQuery;
$(document).ready(function() 
{
	var isMobile = false;
	
	$(window).load(function() 
	{
		//$("#wait_message").addClass('hidden');
		//show_favs();
    });
    
    $(window).resize(function() 
	{
		if ($("#results").hasClass('master')) 
		{
			setView('');
			if(isMobile)
			{
				getClasses('mobile');
			}
			else
			{
				getClasses('desktop');
			}
		}
    });
    
    /******************
    * CLICK LISTENERS *
    ******************/
	
	$("#print_btn").click(function(){
		print_fav_list();
	});
	
	$('#results').on('click', '.class.preview', function() {
      $(this).parent().find('.class.data').slideToggle(500, 'swing');
      toggleArrow($(this));
    });
	
	
	$("#master_f").click(function()
	{
		window.location.href = "https://web.otc.edu/schedules/master-schedule/";
	});
	
	
	$("#advanced_search_f").click(function()
	{
		window.location.href = "https://web.otc.edu/schedules/search/";
	});
	
	$("#reg_dates").click(function()
	{
		window.location.href = "https://students.otc.edu/registrar/registration-dates/";
	});
	
	$("#finals").click(function()
	{
		window.location.href = "https://academics.otc.edu/academicservices/final-exam-schedule/";
	});
	
	$('#results').on('click', '.class.fav', function() 
	{
		if ($(this).hasClass('active')) 
		{
			removeFav($(this).attr('class').split(' ')[2]);
			$( this ).attr('title', 'Click here to add this class to your fav list.');
		} 
		else 
		{
			addFav($(this).attr('class').split(' ')[2]);
			$( this ).prop('title', 'Click here to remove this class from your fav list.');
		}
		$(this).toggleClass('active');
    });
	
	$("#checkout").bind('click', function()
	{
		checkout_favs();	
	});
	
	/*******************
    * CHANGE LISTENERS *
    *******************/
		
	
	
	/************
    * FUNCTIONS *
    ************/
    
	function setView(view) 
	{
		var width = $(window).width();
      
		if(view == 'solid') 
		{
			//getClasses('desktop')
		} 
		else if(view == 'liquid') 
		{
			//getClasses('mobile');
		} 
		else 
		{
			if (width <= 800) 
			{
				if ($('#mobile').length) 
				{
					if ($('desktop').length) 
					{
						$('#desktop').addClass('hidden');
					}
					$('#mobile').removeClass('hidden');
				} 
				else 
				{
					//getClasses('mobile');
					isMobile = true;
				}
			} 
			else 
			{
				if ($('#desktop').length) 
				{
					if ($('#mobile').length) 
					{
						$('#mobile').addClass('hidden');
					}
					$('#desktop').removeClass('hidden');
				} 
				else 
				{
					//getClasses('desktop');
					isMobile = false;
				}
				isMobile = false;
			}
		}
	}
	
	function toggleArrow(parent) 
	{
		var arrow = parent.find('span:nth-child(1)');
      
		if (arrow.hasClass('active')) 
		{
			arrow.html('&#9656;');
			arrow.removeClass('active');
		} 
		else 
		{
			arrow.html('&#9662;');
			arrow.addClass('active');
		}
    }	
	
	
	function show_favs()
	{
		getFavs();
	}
	
	/* COOKIES!!! */
	
	function addFav(fav) 
	{
		var jar = ($.cookie('otc_favlist')) ? $.cookie('otc_favlist').split(',') : [];
		var cookies = '';

		if($.inArray(fav, jar) == -1) 
		{
			jar.push(fav);
		}

		for(var i = 0; i < jar.length; i++) 
		{
			cookies += (i > 0) ? ',' + jar[i] : jar[i];
		}

		$.cookie('otc_favlist', cookies, {path: '/schedules'});
    }
    
    function removeFav(fav) 
	{
		var jar = ($.cookie('otc_favlist')) ? $.cookie('otc_favlist').split(',') : '';
		var cookies = '';

		if($.inArray(fav, jar) != -1) 
		{
			jar.splice($.inArray(fav, jar), 1);
		}

		for(var i = 0; i < jar.length; i++) 
		{
			cookies += (i > 0) ? ',' + jar[i] : jar[i];
		}

		$.cookie('otc_favlist', cookies, {path: '/schedules'});
    }
    
    function checkout_favs() 
	{
		var jar = ($.cookie('otc_favlist')) ? $.cookie('otc_favlist').split(',') : '';
		var url = 'https://central.otc.edu/Student/Planning/Courses/Search?keyword=';
		
		if (jar.length > 0) 
		{
			for(var i = 0; i < jar.length; i++) 
			{
				var course_parts = jar[i].split("-");
				var course_chunk = course_parts[0]+"-"+course_parts[1]+"-"+course_parts[2];
				
					//url += jar[i] + '%20';
					url += course_chunk + '%20';
			}
			//window.location.href = url;
			window.open(url);
		}
    }
	
	function getFavs() 
	{
		$.get('https://web.otc.edu/media/themes/otc/schedules/search_func.php', 'func=fav&otc_favlist='+checkCookie()+"&"+new Date().toString(), function(data) 
		{
			$('#results').html(data);
		}).done(function() 
		{
			if($("#results").hasClass('hidden'))
			{
				$("#results").removeClass('hidden');
			}
		});
    }
	
	function checkCookie()
	{
		var jar = ($.cookie('otc_favlist')) ? $.cookie('otc_favlist').split(',') : '';
		return jar;
	}
	
	function print_fav_list()
	{
		var fav_data = "";
		$.get('https://web.otc.edu/media/themes/otc/schedules/search_func.php', 'func=prnt&otc_favlist='+checkCookie()+"&"+new Date().toString(), function(data) 
		{
			fav_data = data;
		}).done(function()
		{
			var html_data = "<!DOCTYPE html><html><head><title>Print Your FAV List</title></head><body>" + fav_data + "</html>";
			var print_window = window.open("", "");
			print_window.document.write(html_data);
			print_window.document.close();
			print_window.window.print();
		});
	}
	
});