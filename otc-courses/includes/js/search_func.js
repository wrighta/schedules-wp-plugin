		/** Edited by Louis F. **/
//var $ = jQuery.noConflict( true );
window.jQuery = window.$ = jQuery;
$(document).ready(function() 
{
	var semester = '&s=17FA';
	var location = '';
	var instructor = '';
	var department = '';
	var course = '';
	var section = '';
	var classType = '';
	var startTime = '';
	var endTime = '';
	var openClosed = '';
	var sortBy = '';
	var isMobile = false;
	
	
	
	
    /******************
    * CLICK LISTENERS *
    ******************/
	
	
	$('[name="results"]').on('click', '.class.preview', function() {
      $(this).parent().find('.class.data').slideToggle(500, 'swing');
      toggleArrow($(this));
    });
	
	
	$('[name="results"]').on('click', '.class.fav', function() 
	{
		if ($(this).hasClass('active')) 
		{
			removeFav($(this).attr('class').split(' ')[2]);
			$( this ).attr('title', 'Click here to add this class to your fav list.');
		} 
		else 
		{
			addFav($(this).attr('class').split(' ')[2]);
			$( this ).prop('title', 'Click here to remove this class from your fav list.');
		}
		$(this).toggleClass('active');
    });
	
	
	
	
	/************
    * FUNCTIONS *
    ************/
    
	
	function toggleArrow(parent) 
	{
		var arrow = parent.find('span:nth-child(1)');
      
		if (arrow.hasClass('active')) 
		{
			arrow.html('&#9656;');
			arrow.removeClass('active');
		} 
		else 
		{
			arrow.html('&#9662;');
			arrow.addClass('active');
		}
    }	
	
	/* COOKIES!!! */
	
	function addFav(fav) 
	{
		var jar = ($.cookie('otc_favlist')) ? $.cookie('otc_favlist').split(',') : [];
		var cookies = '';

		if($.inArray(fav, jar) == -1) 
		{
			jar.push(fav);
		}

		for(var i = 0; i < jar.length; i++) 
		{
			cookies += (i > 0) ? ',' + jar[i] : jar[i];
		}

		$.cookie('otc_favlist', cookies, {path: '/schedules'});
    }
    
    function removeFav(fav) 
	{
		var jar = ($.cookie('otc_favlist')) ? $.cookie('otc_favlist').split(',') : '';
		var cookies = '';

		if($.inArray(fav, jar) != -1) 
		{
			jar.splice($.inArray(fav, jar), 1);
		}

		for(var i = 0; i < jar.length; i++) 
		{
			cookies += (i > 0) ? ',' + jar[i] : jar[i];
		}

		$.cookie('otc_favlist', cookies, {path: '/schedules'});
    }
	
	function checkCookie()
	{
		var jar = ($.cookie('otc_favlist')) ? $.cookie('otc_favlist').split(',') : '';
		return jar;
	}
	
});