<?php
	
	function otc_course($atts = [], $content = null, $tag = '')
	{
		$atts = array_change_key_case((array)$atts, CASE_LOWER);
		$course_atts = shortcode_atts([
                                     'code' => '',
									 'number' => ''
                                 ], $atts, $tag);
 
		$return_code = otc_get_course_info($course_atts['code'], $course_atts['number']);
		return $return_code;
	}
	
	add_shortcode('otc_courses', 'otc_course');
	
?>