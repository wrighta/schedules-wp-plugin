<?php
/*
Plugin Name:  Display Courses
Plugin URI:   https://web.otc.edu
Description:  Display OTC courses through the use of a shortcode
Version:      1.0
Author:       Aaron Wright
*/

wp_register_script( 'otc-courses-js', plugin_dir_url( __FILE__ ).'includes/js/search_func.js' , '', '', true );
wp_register_script( 'otc-cookie-js', plugin_dir_url( __FILE__ ).'includes/js/cookie.js' , '', '', true );
wp_register_style( 'otc-courses-css', plugin_dir_url( __FILE__ ).'includes/css/search.css' , '', '', all );
/***************************************
* includes
****************************************/
	
include('includes/data-getting.php'); // This controls all getting of data for display
include('includes/shortcode-functions.php'); // The shortcodes and shortcode functions

?>